import {
  Route,
  Routes,
  BrowserRouter as Router,
  Navigate,
} from "react-router-dom";
import { useState } from "react";

import QuotesDetail from "./pages/QuotesDetail";
import AllQuotes from "./pages/AllQuotes";
import AddQuotes from "./pages/AddQuotes";
import MainNavigation from "./pages/MainNavigation";

function App() {
  const [quote, setQuote] = useState([]);

  const addNewQuote = (newQuote) => {
    setQuote((prev) => {
      return [...prev, newQuote];
    });
  };

  return (
    <Router>
      <header>
        <MainNavigation />
      </header>
      <div>
        <AddQuotes quote={quote} />
      </div>

      <Routes>
        <Route exact path="/" element={<Navigate to="/all-quotes" />} />
        <Route path="/all-quotes" element={<AllQuotes />} />
        <Route path="/add-quotes" element={<AddQuotes />} />
        <Route path="/add-quotes/:quoteId" element={<QuotesDetail />} />
      </Routes>
    </Router>
  );
}

export default App;
