import React from "react";
import { NavLink } from "react-router-dom";

const MainNavigation = () => {
  return (
    <header>
      <div>
        <nav>
          <ul>
            <li>
              <NavLink to="/all-quotes">All Quotes</NavLink>
            </li>
            <li>
              <NavLink to="/add-quotes">Add Quotes</NavLink>
            </li>
          </ul>
        </nav>
      </div>
    </header>
  );
};
export default MainNavigation;
