import React, { useState } from "react";

const AddQuotes = (props) => {
  const [newQuote, setNewQuote] = useState({
    quote: " ",
  });

  const SubmitButton = (event) => {
    event.preventDefault();

    props.addNewQuote(newQuote);
  };

  const addQuote = (event) => {
    setNewQuote((prev) => {
      const newValue = { ...prev };
      newValue.quote = event.target.value;
      return newValue;
    });
  };

  return (
    <div>
      <p>Add quotes</p>
      <form onSubmit={SubmitButton}>
        <label>Quote:</label>
        <input
          type="text"
          id="add-quote"
          placeholder="Add a quote"
          onChange={addQuote}
        ></input>
        <button type="button"> Submit </button>
      </form>
    </div>
  );
};
export default AddQuotes;
